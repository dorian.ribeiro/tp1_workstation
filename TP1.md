# Maitrise de poste

## Index 

<!-- vim-markdown-toc GitLab -->

* [Self-footprinting](#self-footprinting)
    * [Host OS](#host-os)
    * [Devices](#devices)
    * [Users](#users)
    * [Processus](#processus)
    * [Network](#network)
* [Scripting](#scripting)
* [Gestion de softs](#gestion-de-softs)
* [Partage de fichiers](#partage-de-fichiers)

<!-- vim-markdown-toc -->

# Self-footprinting

## Host OS
* nom de la machine
    * On vas effectuer la commande "hostname" ce qui vas nous donner le nom de l'ordinateur    
    ```
    PS C:\Users\doria> hostname
    LAPTOP-88MUS3CI
* OS et version
    * La commande "(Get-WmiObject -class Win32_OperatingSystem).Caption" nous sert a trouver le nom de l'os 
    ```
    PS C:\Users\doria> (Get-WmiObject -classWin32_OperatingSystem).Caption
    Microsoft Windows 10 Famille
    ```
    * La commande "[System.Environment]::OSVersion.Version" nous sert a trouver la version de l'os  
    ```
    PS C:\Users\doria> [System.Environment]::OSVersion.Version

    Major  Minor  Build  Revision
    -----  -----  -----  --------
    10     0      19041  0

* architecture processeur (32-bit, 64-bit, ARM, etc)
    * Pour chercher l'arcgitecture on effectue la commande "(Get-WmiObject CIM_OperatingSystem).OSArchitecture"
    ```
    PS C:\Users\doria> (Get-WmiObject CIM_OperatingSystem).OSArchitecture
    64 bits
    

* quantité RAM et modèle de la RAM
    * Pour avoir la quantité de ram et le nom de la ram on utilise "Get-WmiObject win32_physicalmemory | Format-Table Manufacturer,Capacity"
    ```
    PS C:\Users\doria> Get-WmiObject win32_physicalmemory | Format-Table Manufacturer,Capacity

    Manufacturer   Capacity
    ------------   --------
    SK Hynix     8589934592


 ## Devices  
* la marque et le modèle de votre processeur
  
   * Pour identifier le nombre de processeur et le nombre de coeur on utilise "Get-WmiObject -Class Win32_Processor | Select-Object -Property Name, Number*"
        ```
        PS C:\Users\doria> Get-WmiObject -Class Win32_Processor | Select-Object -Property Name, Number*
        
     Name                                     NumberOfCores       NumberOfEnabledCore NumberOfLogicalProcessors
       ----                                     ------------- --    ----------------- -------------------------
       Intel(R) Core(TM) i5-9300H CPU @ 2.40GHz             4                   4                         8
  
  * si c'est un proc Intel, expliquer le nom du processeur
  Intel® Core™ i5-9300H CPU @ 2,40GHz  
    Nom de la marque "Intel® Core™"  
    version du processeur "i5-9300H"  
    Nombre d’opération par seconde "2,40GHz"
    

* la marque et le modèle :
  * de votre touchpad/trackpad
  
  * de votre carte graphique
      Grace a cette commande   
      "wmic path win32_VideoController get name"  
      on obtient  
      ```
      PS C:\Users\doria> wmic path win32_VideoController get name
    Name
      Intel(R) UHD Graphics 630
    NVIDIA GeForce GTX 1660 Ti
      


* identifier la marque et le modèle de votre(vos) disque(s) dur(s)
    * Avec "Get-Disk" on obtient
    ```
    PS C:\Users\doria> Get-Disk

    Number Friendly Name Serial Number                    HealthStatus         OperationalStatus      Total Size Partition
                                                                                                             Style
    ------ ------------- -------------                    ------------         -----------------      ---------- ----------
    0      SAMSUNG MZ... 0025_3889_91CC_1035.             Healthy              Online                  238.47 GB GPT
    1      ST1000LM03...             WKP5LTQK             Healthy              Online                  931.51 GB GPT

* identifier les différentes partitions de votre/vos disque(s) dur(s)
    * Grace a "Get-Partition" on vas pouvoir afficher toute les partition présente sur les disque
    ```
    PS C:\Users\doria> Get-Partition


   DiskPath : \\?\scsi#disk&ven_nvme&prod_samsung_mzvlb256#5&14fa181f&0&000000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

    PartitionNumber  DriveLetter Offset                                        Size Type
    ---------------  ----------- ------                                        ---- ----
    1                           1048576                                     260 MB System
    2                           273678336                                    16 MB Reserved
    3                C           290455552                                237.23 GB Basic
    4                           255011586048                               1000 MB Recovery


   DiskPath : \\?\scsi#disk&ven_st1000lm&prod_035-1rk172#4&381df47e&0&000400#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

    PartitionNumber  DriveLetter Offset                                        Size Type
    ---------------  ----------- ------                                        ---- ----
    1                           17408                                     15.98 MB Reserved
    2                D           16777216                                  931.5 GB Basic


* déterminer le système de fichier de chaque partition
    * "get-disk"
    ```
    PS C:\WINDOWS\system32> get-disk

    Number Friendly Name Serial Number                    HealthStatus         OperationalStatus      Total Size   Partition
                                                                                                                     Style
    ------ ------------- -------------                    ------------         -----------------      ----------     ----------
    0      SAMSUNG MZ... 0025_3889_91CC_1035.             Healthy              Online                  238.47 GB     GPT
    1      ST1000LM03...             WKP5LTQK             Healthy              Online                  931.51 GB     GPT
* expliquer la fonction de chaque partition
    * GPT = C'est une nouvelle norme qui permet de remplacer progressivement le vieillissent MBR. Elle est associée à UEFI qui lui, remplace le BIOS


## Users 
* la liste **complète** des utilisateurs de la machine (je vous vois les Windowsiens...)
    * Avec "net user" on obtient
    ```
    PS C:\WINDOWS\system32> net user

    comptes d’utilisateurs de \\LAPTOP-88MUS3CI

    -------------------------------------------------------------------------------
    Administrateur           DefaultAccount           doria
    Invité                   WDAGUtilityAccount
    La commande s’est terminée correctement.

* déterminer le nom de l'utilisateur qui est full admin sur la machine
  * il existe toujours un utilisateur particulier qui a le droit de tout faire sur la machine
  * pour les Windowsiens : faites des recherches sur `NT-AUTHORITY\SYSTEM` et le concept de SID

## Processus
* choisissez 5 services système et expliquer leur utilité
  * par "service système" j'entends des processus élémentaires au bon fonctionnement de la machine
-audiodg.exe - Isolation graphique de périphérique audio Windows   
-dasHost.exe - Device Association Framework Provider Host   
-Helppane.exe est un fichier associé au système d'assistance de Platform Support Client des systèmes d'exploitation Windows  
-settingSyncHost.exe - Host Process for Setting Synchronization  
-wininit.exe -Application de démarrage de Windows  
  * sans eux, l'OS tel qu'on l'utilise n'existe pas
  * exemple : interface graphique, démon réseau, etc.
* déterminer les processus lancés par l'utilisateur qui est full admin sur la machine

## Network
Afficher la liste des cartes réseau de votre machine
```
PS C:\WINDOWS\system32> Get-NetAdapter

Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
Wi-Fi                     Intel(R) Wireless-AC 9560 160MHz             18 Disconnected C0-B8-83-04-AB-32          0 bps
Ethernet                  Realtek PCIe GbE Family Controller           16 Up           F8-75-A4-29-C1-19         1 Gbps
```
* expliquer la fonction de chacune d'entre elles
  * exemple : carte WiFi, carte de loopback, etc.
La carte Wifi sert a se connecter a distance a un réseau
La carte Ethernet sert a se connecter a un réseau filaire

* déterminer quel programme tourne derrière chacun des ports
```
    PS C:\WINDOWS\system32> netstat -a -b
 [Discord.exe]
     TCP    127.0.0.1:22885        LAPTOP-88MUS3CI:0      LISTENING
 [Battle.net.exe]
  TCP    127.0.0.1:49742        LAPTOP-88MUS3CI:65001  ESTABLISHED
 [nvcontainer.exe]
  TCP    127.0.0.1:49744        LAPTOP-88MUS3CI:0      LISTENING
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:49744        LAPTOP-88MUS3CI:49762  ESTABLISHED
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:49762        LAPTOP-88MUS3CI:49744  ESTABLISHED
 [NVIDIA Share.exe]
  TCP    127.0.0.1:49803        LAPTOP-88MUS3CI:49804  ESTABLISHED
 [Battle.net.exe]
  TCP    127.0.0.1:49804        LAPTOP-88MUS3CI:49803  ESTABLISHED
 [Battle.net.exe]
  TCP    127.0.0.1:50305        LAPTOP-88MUS3CI:0      LISTENING
  ```
* expliquer la fonction de chacun de ces programmes
[Discord.exe] = Application de discussion en ligne
[Battle.net.exe] = Apllication de jeux
[nvcontainer.exe] = Le processus appelé NVIDIA Container appartient au logiciel NVIDIA Container ou NVIDIA LocalSystem Container ou aux pilotes NVIDIA ou au panneau de contrôle d'affichage NVIDIA de NVIDIA
[NVIDIA Web Helper.exe] = Le processus appelé NVIDIA Web Helper Service appartient au logiciel Node.js ou au logiciel NVIDIA Display Control Panel de Node.js.
[NVIDIA Share.exe] = Le processus appelé NVIDIA Share appartient au logiciel NVIDIA Share de la compagnie NVIDIA

# III. Gestion de softs

Tous les OS modernes sont équipés ou peuvent être équipés d'un gestionnaire de paquets. Par exemple :
* `apt` pour les GNU/Linux issus de Debian
* `dnf` pour les GNU/Linux issus de RedHat
* `brew` pour macOS
* `chocolatey` pour Windows

🌞 Expliquer l'intérêt de l'utilisation d'un gestionnaire de paquets
* par rapport au téléchargement en direct sur internet
    * Telechargement plus rapide par un serveur ou tous les logitiel sont centraliser
* penser à l'identité des gens impliqués dans un téléchargement (vous, l'éditeur logiciel, etc.)
    * Editeur de chocolatey et l'installateur du logitiel
* penser à la sécurité globale impliquée lors d'un téléchargement
    * Aucun virus dans la base de donnée du serveur

🌞 Utiliser un gestionnaire de paquet propres à votre OS pour
* lister tous les paquets déjà installés
* déterminer la provenance des paquets (= quel serveur nous délivre les paquets lorsqu'on installe quelque chose)
```
PS C:\WINDOWS\system32> choco list -l -detail
Parsing -detail resulted in error (converted to warning):
 Cannot bundle unregistered option '-e'.
Chocolatey v0.10.15
2 validations performed. 1 success(es), 0 warning(s), and 0 error(s).

chocolatey 0.10.15
 Title: Chocolatey | Published: 02/11/2020
 ...
 Software Site: https://github.com/chocolatey/choco
 Software License: https://raw.githubusercontent.com/chocolatey/choco/master/LICENSE
 ...
1 packages installed.
# IV. Machine virtuelle

**Le but est de créer une machine virtuelle fonctionnelle à laquelle on peut se connecter en SSH.**
* "machine virtuelle" : virtualiser un PC dans votre PC (très pratique pour tester des choses, ou répartir des ressources)
* "SSH" : protocole qui permet de contrôler une machine à distance

Une connexion SSH, c'est quelque chose que l'on fait quotidiennement, peu importe votre métier dans l'informatique.

---

Vous aurez besoin d'un hyperviseur pour cela. Je vous conseille [VirtualBox](https://www.virtualbox.org/), je ne vous apporterai pas ou peu de support si vous en choisissez un autre.

Vous aurez aussi besoin de l'image d'un OS à installer. Nous installerons ici CentOS 7, que vous pourrez [DL par ici](http://miroir.univ-paris13.fr/centos/7.8.2003/isos/x86_64/CentOS-7-x86_64-Minimal-2003.iso) (miroir officiel).

CentOS7 est un OS GNU/Linux alors adaptez les commandes que vous tapez.

## 0. Configuration de VirtualBox

Créer un réseau privé-hôte *(host-only)* qui porte l'adresse `192.168.120.1`. Cochez la case pour activer le serveur DHCP dans ce réseau.

## 1. Installation de la VM

Créer une machine virtuelle dans VirtualBox avec les paramètres par défaut à part :
* deux cartes réseau 
  * une carte NAT
  * une carte host-only

## 2. Installation de l'OS

Démarrer la machine dans VirtualBox et insérer le `.iso` comme un CD.

Réaliser l'installation de CentOS 7. Presque tout par défaut, les choses à configurer sont les suivantes :
* **langue : anglais**
* clavier : français (azerty ou mac)
* partitionnement par défaut (il faut rentrer dans le menu et faire "Done" pour valider le partitionnement par défaut)
* activer les cartes réseau
* une fois l'installation lancée, créer un utilisateur et définir un mot de passe root
  * n'oubliez pas de cocher la case "faire de cet utilisateur un administrateur" quand vous définissez le mot de passe de l'utilisateur

> `root` c'est le super-utilisateur dans MacOS et GNU/Linux.

## 3. Configuration post-install

Allumer la machine, puis se connecter avec root (ou l'utilisateur créé à l'installation).

Vous pouvez lister les cartes réseau avec :
```bash
$ ip a
```

> Les noms des cartes réseau ressembleront à `enp0s3`, `enp0s8`, etc.

Vous pouvez allumer les cartes réseau avec : 
```bash
$ ifup <CARTE RESEAU>

# Par exemple
$ ifup enp0s8
```

Assurez-vous que les deux cartes réseau sont allumées (elles sont allumée si elles ont une IP).

Effectuez une connexion SSH de votre PC, vers la machine virtuelle, en utilisant le terminal de VOTRE PC (donc votre Powershell si vous êtes sur Windows) :
```bash
# Connexion sur la machine 10.10.10.33, en tant que l'utilisateur Bob :
$ ssh bob@10.10.10.33
```

```
PS C:\WINDOWS\system32> ssh root@192.168.120.50
root@192.168.120.50's password:
Last login: Mon Nov  9 16:51:30 2020
[root@localhost ~]#
```

## 4. Partage de fichiers

### Créer le serveur (sur votre PC)

> Rappel : dans le rendu, la marche à suivre exacte et exhaustive pour réaliser les étapes du TP doit être présente.

Créer un partage de fichiers **SUR VOTRE PC** (pas dans la VM) :
* si vous avez Windows, ce sera avec Samba
  * un simple clic-droit > propriétés
* sous GNU/Linux je vous conseille de faire un partage NFS (ou Samba)
* sous MacOS, vous pouvez aussi faire un partage NFS

### Accéder au partage depuis la VM

Accéder au partage de fichiers depuis la machine virtuelle
* le dossier partagé doit être accessible dans la VM
* vous devez donc pouvoir créer, éditer, et supprimer des fichiers depuis la machine virtuelle
* vous devez prouver que tout ceci fonctionne

> N'hésitez pas à me demander de l'aide pour cette partie.

Marche à suivre (pour les gens sous Windows). **A faire DANS la VM :**
```bash
# Installer le paquet qui permet d'utiliser les partage Samba
$ yum install -y cifs-utils

# Créer un dossier où on accédera au partage
$ mkdir /opt/partage

# Monter le partage dans la VM
$ mount -t cifs -o username=<VOTRE_UTILISATEUR>,password=<VOTRE_MOT_DE_PASSE> //<IP_DE_VOTRE_PC>/<NOM_DU_PARTAGE> /opt/partage
# Par exemple, si :
# - l'utilisateur de votre PC s'appelle Bob
# - votre mot de passe est "toto"
# - que le partaget que vous avez créé s'appelle "share"
# - que votre PC a l'IP 192.168.120.1 (comme j'ai demandé dans le TP)
# la commande sera alors :
$ mount -t cifs -o username=Bob,password=toto //192.168.120.1/share /opt/partage
```

> **ATTENTION** le "username=Bob,password=toto" on parle d'un utilisateur qui existe sur VOTRE PC (pas dans la VM).
